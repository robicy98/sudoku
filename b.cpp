#include <stdio.h>
#include <iostream>
#include <fstream>
#include <time.h>

using namespace std;

int numberOfSolutions;

int Sudoku[9][9];

int resolvedSudoku[9][9];

void copy(){
    for (int i = 0; i < 9; i++)
        for (int j = 0;j < 9; j++){
            resolvedSudoku[i][j] = Sudoku[i][j];
        }
}

bool GetLocation(int &row, int &col)
{
    for (row = 0; row < 9; row++)
        for (col = 0; col < 9; col++)
            if (Sudoku[row][col] == 0)
                return true;
    return false;
}

bool UsedInRowOrCol(int row,int col,int num){
    for (int i = 0; i < 9; i++){
        if (Sudoku[row][i] == num)
            return true;
        if (Sudoku[i][col] == num)
            return true;
    }
    return false;
}

bool UsedIn3x3(int boxStartRow, int boxStartCol, int num)
{
    for (int row = 0; row < 3; row++)
        for (int col = 0; col < 3; col++)
            if (Sudoku[row+boxStartRow][col+boxStartCol] == num)
                return true;
    return false;
}

bool isSafe(int row, int col, int num)
{
    return !UsedInRowOrCol(row, col, num) &&
           !UsedIn3x3(row - row%3 , col - col%3, num);
}

bool SolveSudoku()
{
    int row, col;

    if (!GetLocation(row, col)){
        numberOfSolutions++;
        if (numberOfSolutions == 2){
            //copy();
            return true;
        }
        else{
            copy();
            return false;
        }
    }

    for (int num = 1; num <= 9; num++)
    {
        if (isSafe(row, col, num))
        {
            Sudoku[row][col] = num;

            if (SolveSudoku()){
                return true;
            }

            Sudoku[row][col] = 0;
        }
    }
    return false;
}

void printSudoku()
{
    for (int row = 0; row < 9; row++)
    {
       for (int col = 0; col < 9; col++)
             cout << Sudoku[row][col] << " ";
        cout << endl;
    }
}

void printCory(){
    for (int row = 0; row < 9; row++)
    {
       for (int col = 0; col < 9; col++)
             cout << resolvedSudoku[row][col] << " ";
        cout << endl;
    }
}

void GenereateRandomSudoku(){
    int row, col, num;
    bool ok;
    for (int row = 0; row < 9; row++)
        for(int col = 0; col < 9 ; col++)
            Sudoku[row][col] = 0;

    for (int i = 0; i < 30 ; i++){
        do{
        row = rand()%9;
        col = rand()%9;
        num = rand()%9+1;
        ok = isSafe(row, col, num);
        }while (!ok);
        Sudoku[row][col] = num;
    }
}

void getUnsolvedSudoku(){
    int row, col;
    for (int i = 0; i < 40; i++){
        do{
            row = rand()%9;
            col = rand()%9;
        }while(Sudoku[row][col]==0);
        Sudoku[row][col] = 0;
    }
}

int main()
{
    srand (time(NULL));
    ifstream f;
    f.open("top95.txt");
    for (int i = 0; i < 50; i++){
        // break;
        numberOfSolutions = 0;
            char c = 0;
            for (int i = 0;i < 9; i++)
                for (int j = 0;j < 9; j++){
                    f >> c;
                    if (c == 46)
                        Sudoku[i][j] = 0;
                        else{
                        Sudoku[i][j] = c - 48;
                        }
                }
        SolveSudoku();
        if (numberOfSolutions == 1)
            //printCory();
        if (numberOfSolutions > 1){
            //printCory();
            cout << "More solutions" << endl;
        }
        if (numberOfSolutions == 0)
        cout << "No solution exists" << endl;
    }
    // do{
    // GenereateRandomSudoku();
    // }while(SolveSudoku() != 1);
    // getUnsolvedSudoku();
    // printSudoku();
    f.close();
    return 0;
}